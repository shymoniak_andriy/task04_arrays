package com.shymoniak;

import com.shymoniak.view.View;

public class App {
    public static void main(String[] args) {
        new View().consoleInterface();
    }
}
