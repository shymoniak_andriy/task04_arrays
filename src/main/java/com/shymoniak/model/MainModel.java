package com.shymoniak.model;

import com.shymoniak.model.defaults.DefaultMethods;
import com.shymoniak.model.task.TaskA;
import com.shymoniak.model.task.TaskB;
import com.shymoniak.model.task.TaskC;
import com.shymoniak.model.task.d.TaskD;

public class MainModel {
    private DefaultMethods defaultMethods = new DefaultMethods();
    private TaskA taskA = new TaskA();
    private TaskB taskB = new TaskB();
    private TaskC taskC = new TaskC();
    private TaskD taskD = new TaskD();


    public void resTaskA() {
        String[] arr1 = defaultMethods.fillArray(50);
        String[] arr2 = defaultMethods.fillArray(50);
        String[] res = taskA.addTwoArrays(arr1, arr2, true);
        defaultMethods.printArray(res);
    }


    public void resTaskB() {
        String[] arr = defaultMethods.fillArrayWithNumbers(50, 30);
        arr = taskB.deleteExcess(arr);
        defaultMethods.printArray(arr);
    }

    public void resTaskC(){
        String[] arr = defaultMethods.fillArrayWithNumbers(50, 3);
        arr = taskC.deleteRepeatingInARow(arr);
        defaultMethods.printArray(arr);
    }

    public void resTaskD(){
        taskD.startGame();
    }
}
