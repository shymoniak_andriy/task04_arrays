package com.shymoniak.model.defaults;

import java.util.Random;

public class DefaultMethods {
    private Random random = new Random();

    public String[] fillArray(int size) {
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {
            arr[i] = "Element " + Math.abs(random.nextInt() % 30);
        }
        return arr;
    }

    public String[] fillArrayWithNumbers(int size, int maxNum) {
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {
            arr[i] = "" + Math.abs(random.nextInt() % maxNum);
        }
        return arr;
    }

    public void printArray(String[] arr) {
        System.out.println();
        for (int i = 1; i <= arr.length; i++) {
            System.out.print("\t" + arr[i-1] + "\t");
            if ((i % 5) == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }
}
