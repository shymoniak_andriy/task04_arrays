package com.shymoniak.model.task;

import com.shymoniak.model.defaults.DefaultMethods;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Видалити в масиві всі числа, які повторюються більше двох разів.
 */
public class TaskB {

    /**
     * Deletes all elements from array, which are repeated more than twice
     *
     * @param arr
     * @return
     */
    public String[] deleteExcess(String[] arr) {
        // Printing initial array in console
        printArray(arr);

        //Converting array given into ArrayList
        ArrayList<String> list = new ArrayList<>(Arrays.asList(arr));
        ArrayList<String> res = new ArrayList<>();

        int counter;
        while (list.size() != 0) {
            counter = getCount(list, list.get(0));
            if (counter < 2) {
                while (counter > 0) {
                    res.add(list.get(0));
                    counter--;
                }
            }
            deleteAll(list, list.get(0));
        }
        return res.toArray(new String[res.size()]);
    }

    /**
     * Counts amount of strings like str in the list
     *
     * @param list
     * @param str
     * @return
     */
    private int getCount(ArrayList<String> list, String str) {
        int counter = 0;
        for (int i = 0; i < list.size(); i++) {
            if (str.equals(list.get(i))) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Deletes all strings like str from the list
     *
     * @param list
     * @param str
     * @return
     */
    private ArrayList<String> deleteAll(ArrayList<String> list, String str) {
        while (list.contains(str)) {
            list.remove(str);
        }
        return list;
    }

    /**
     * Prints array given in console
     *
     * @param arr
     */
    public void printArray(String[] arr) {
        System.out.println("Initial array looks as follows: ");
        new DefaultMethods().printArray(arr);
        System.out.println("\n-----------------------------------------------");
    }
}
