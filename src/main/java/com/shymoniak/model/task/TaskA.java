package com.shymoniak.model.task;

import com.shymoniak.model.defaults.DefaultMethods;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Дано два масиви. Сформувати третій масив, що складається
 * з тих елементів, які:
 * а) присутні в обох масивах;
 * б) присутні тільки в одному з масивів.
 */
public class TaskA {

    /**
     * Makes an array out of two arrays, where all elements are unique or similar
     * depending on user request
     *
     * @param arr1
     * @param arr2
     * @param similar
     * @return
     */
    public String[] addTwoArrays(String[] arr1, String[] arr2, boolean similar) {
        // Printing initial arrays
        printArray(arr1, 1);
        printArray(arr2, 2);

        //Converting arrays given into ArrayLists
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList(arr1));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList(arr2));

        // Makes all elements in both lists unique, so that they didn't repeat
        list1 = makeUnique(list1);
        list2 = makeUnique(list2);

        if (similar) {
            System.out.println("Similar elements:");
            return findSimilar(list1, list2);
        } else {
            System.out.println("Unique elements: ");
            return findDifferent(list1, list2);
        }
    }

    /**
     * Finds all similar elements from both lists
     *
     * @param list1
     * @param list2
     * @return String array filled with similar elements
     */
    private String[] findSimilar(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<String> res = new ArrayList<>();

        for (String str1 : list1) {
            for (String str2 : list2) {
                if (str1.equals(str2)) {
                    res.add(str1);
                }
            }
        }
        // Converts ArrayList<String> into String array
        return res.toArray(new String[res.size()]);
    }

    /**
     * Finds all unique elements from both lists
     *
     * @param list1
     * @param list2
     * @return String array filled with unique elements
     */
    private String[] findDifferent(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<String> res = new ArrayList<>();

        // Removes similar elements from list1 and list2
        ArrayList<ArrayList<String>> modifiedLists = removeSimilar(list1, list2);
        list1 = modifiedLists.get(0);
        list2 = modifiedLists.get(1);

        res.addAll(list1);
        res.addAll(list2);

        // Converts ArrayList<String> into String array
        return res.toArray(new String[res.size()]);
    }

    /**
     * Removes all similar elements from ArrayList
     *
     * @param list
     * @return ArrayList where all elements are unique
     */
    private ArrayList<String> makeUnique(ArrayList<String> list) {
        ArrayList<String> res = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (!res.contains(list.get(i))) {
                res.add(list.get(i));
            }
        }
        return res;
    }

    /**
     * Removes similar elements between ArrayLists list1 and list2
     *
     * @param list1
     * @param list2
     * @return ArrayList of two ArrayLists, which contain only different elements
     */
    private ArrayList<ArrayList<String>> removeSimilar(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<ArrayList<String>> res = new ArrayList<>();
        String commonWord;
        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                // To prevent Exception if one of lists is empty
                if (list1.size() != 0 && list2.size() != 0) {
                    if (list1.get(i).equals(list2.get(j))) {
                        commonWord = list1.get(i);
                        // Deletes common word from both lists
                        while (list1.contains(commonWord)) {
                            list1.remove(commonWord);
                            if (i < list1.size()) {
                                i++;
                            }
                        }
                        while (list2.contains(commonWord)) {
                            list2.remove(commonWord);
                            j++;
                            if (j < list2.size()) {
                                j++;
                            }
                        }
                    }
                }
            }
        }
        res.add(list1);
        res.add(list2);
        return res;
    }

    /**
     * Prints array given in console
     *
     * @param arr
     * @param number
     */
    public void printArray(String[] arr, int number) {
        System.out.println("Initial array №" + number + " looks as follows: ");
        new DefaultMethods().printArray(arr);
        System.out.println("\n-----------------------------------------------");
    }

}
