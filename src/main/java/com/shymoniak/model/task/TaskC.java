package com.shymoniak.model.task;

import com.shymoniak.model.defaults.DefaultMethods;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Знайти в масиві всі серії однакових елементів, які йдуть підряд, і
 * видалити з них всі елементи крім одного.
 */
public class TaskC {

    /**
     * Finds and deletes all elements from array given, which are going
     * in a row.
     *
     * @param arr
     * @return
     */
    public String[] deleteRepeatingInARow(String[] arr) {
        // Printing initial array in console
        printArray(arr);

        //Converting array given into ArrayList
        ArrayList<String> list = new ArrayList<>(Arrays.asList(arr));

        int oldSize = list.size();
        for (int i = 0; i < list.size(); i++) {
            if ((i + 1) < list.size()) {
                if (list.get(i).equals(list.get(i + 1))) {
                    deleteFromIndex(list, i);
                }
            }
        }

        /* After elements in a row are deleted, there can appear new row of elements.
         * To prevent that we are checking whole array again and doing recursion of
         * this method.
         */
        if (oldSize > list.size()) {
            System.out.println("Checking for new elements going in a row after deletion");
            deleteRepeatingInARow(list.toArray(new String[list.size()]));
        }
        return list.toArray(new String[list.size()]);
    }

    /**
     * Deletes elements row in a list starting from index
     *
     * @param list
     * @param index
     * @return
     */
    private ArrayList<String> deleteFromIndex(ArrayList<String> list, int index) {
        String temp;
        for (int i = index; i < list.size(); i++) {
            temp = list.get(index);
//            list.set(index, "*"); good way to check if elements are deleted right
            list.remove(index);
            if ((index + 1) < list.size()) {
                index++;
                while (temp.equals(list.get(index))) {
//                    list.set(index, "*"); good way to check if elements are deleted right
                    list.remove(index + 1);
                    if ((index + 1) < list.size()) {
                        index++;
                    }
                }
            }
            break;
        }
        return list;
    }

    /**
     * Prints array given in console
     *
     * @param arr
     */
    private void printArray(String[] arr) {
        System.out.println("Array looks as follows: ");
        new DefaultMethods().printArray(arr);
        System.out.println("\n-----------------------------------------------");
    }
}
