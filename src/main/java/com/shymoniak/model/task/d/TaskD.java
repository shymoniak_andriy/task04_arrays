package com.shymoniak.model.task.d;

/**
 * Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому залі,
 * з якого ведуть 10 закритих дверей. За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів, або монстр, який має силу від 5 до 100 балів, з яким герою
 * потрібно битися. Битву виграє персонаж, що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 * 1. Організувати введення інформації про те, що знаходиться за дверима, або заповнити її,
 * використовуючи генератор випадкових чисел.
 * 2. Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
 * 3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
 * 4. Вивести номери дверей в тому порядку, в якому слід їх відкривати герою, щоб залишитися в живих,
 * якщо таке можливо.
 */
public class TaskD {

    private static final int INITIAL_STRENGTH = 25;
    private int heroStrength = INITIAL_STRENGTH;
    private int localDeathCounter = 0;
    private int localDoorsCounter = 0;
    private RandomMethods randomMethods = new RandomMethods();


    public void startGame() {
        String[][] arr = fillArray();
        printArray(arr);
        System.out.println("\n\nOur hero is weaker than " + countDeath(arr, heroStrength) + " monsters\n");

        while (artifactsLeft(arr)) {
            arr = takeArtifact(arr);
        }
        int tempIndex;
        while (monstersLeft(arr)){
            tempIndex = searchForWeakestMonster(arr);
            if (canWin(heroStrength, arr[tempIndex][1])){
                System.out.println("Hero[" + heroStrength + "]" + " beats the monster[" + arr[tempIndex][1] + "]");
                arr = killMonster(arr, tempIndex);
            } else{
                System.err.println("You can't win all monsters");
                System.err.println("Your hero strength = " + heroStrength);
                System.err.println("Next monster strength = " + arr[tempIndex][1]);
                break;
            }
        }
        System.out.println("\n=========================================\n");
        System.out.println("Results of the game:");
        printArray(arr);
        System.out.println();
    }

    /**
     * Fills array [10][2] with values:
     * [0-9][0] - true/false - monster behind the door or magical artifact
     * [0-9][1] - numbers - if monster - monster strength
     * - if artifact - artifacts bonus
     *
     * @return
     */
    private String[][] fillArray() {
        String[][] arr = new String[10][2];
        for (int i = 0; i < arr.length; i++) {
            arr[i][0] = "" + randomMethods.isMonsterBehindTheDoor();
            if (arr[i][0].equals("true")) {
                arr[i][1] = "" + randomMethods.getMonsterStrength();
            } else if (arr[i][0].equals("false")) {
                arr[i][1] = "" + randomMethods.getArtifactBonus();
            }
        }
        return arr;
    }

    /**
     * Prints array of doors in console
     *
     * @param arr
     */
    private void printArray(String[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print("\nDoor №" + (i + 1) + "[");
            System.out.print("is monster? " + arr[i][0] + ", ");
            System.out.print("strength: " + arr[i][1] + "]");
        }
    }

    /**
     * Tells whether hero can kill the monster or not
     *
     * @param hero
     * @param monster
     * @return
     */
    private boolean canWin(int hero, String monster) {
        return (hero >= Integer.parseInt(monster));
    }

    /**
     * Counts how many monsters can beat our hero
     *
     * @param arr
     * @return
     */
    private int countDeath(String[][] arr, int heroStrenght) {
        if (localDoorsCounter < arr.length) {
            if (arr[localDoorsCounter][0].equals("true")) {
                if (!canWin(heroStrenght, arr[localDoorsCounter][1])) {
                    localDeathCounter++;
                }
            }
            localDoorsCounter++;
            return countDeath(arr, heroStrenght);
        } else {
            return localDeathCounter;
        }
    }

    private String[][] takeArtifact(String[][] arr) {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals("false")) {
                heroStrength = Integer.parseInt(arr[i][1]) + heroStrength;
                markDone(arr, i);
                counter++;
                break;
            }
        }
        if (counter == 0) {
            System.err.println("There are no artifacts left");
        }
        return arr;
    }

    /**
     * Tells if there are any artifacts left
     *
     * @param arr
     * @return
     */
    private boolean artifactsLeft(String[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals("false")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Searches for monster with the lowest strenght
     * @param arr
     * @return
     */
    private int searchForWeakestMonster(String[][] arr) {
        int weakestIndex = -1;
        int lowestStrength = 999;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals("true")) {
                if (lowestStrength > Integer.parseInt(arr[i][1])) {
                    lowestStrength = Integer.parseInt(arr[i][1]);
                    weakestIndex = i;
                }
            }
        }
        return weakestIndex;
    }

    private String[][] killMonster(String[][] arr, int index){
        markDone(arr, index);
        return arr;
    }

    /**
     * Tells if there are any monsters left
     *
     * @param arr
     * @return
     */
    private boolean monstersLeft(String[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals("true")) {
                return true;
            }
        }
        return false;
    }

    /**
     * If magical artifact is taken or monster is beaten, marks it in
     * the array, so that we didn't make the same steps again
     *
     * @param arr
     * @param index
     * @return
     */
    private String[][] markDone(String[][] arr, int index) {
        arr[index][0] = "DONE";
        arr[index][1] = "DONE";
        return arr;
    }
}
