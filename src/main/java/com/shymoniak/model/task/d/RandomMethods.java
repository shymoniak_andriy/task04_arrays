package com.shymoniak.model.task.d;

import java.util.Random;

public class RandomMethods {
    Random random = new Random();

    /**
     * This class returns boolean value whether there is a
     * monster behind the door or magical artifact
     * @return
     */
    protected boolean isMonsterBehindTheDoor(){
        return random.nextBoolean();
    }

    protected int getArtifactBonus(){
        return Math.abs(random.nextInt() % 71) + 10;
    }

    protected int getMonsterStrength(){
        return Math.abs(random.nextInt() % 96) + 5;
    }
}
