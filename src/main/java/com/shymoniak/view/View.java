package com.shymoniak.view;

import com.shymoniak.controller.Controller;

import java.util.Scanner;

public class View {
    Scanner scan = new Scanner(System.in);
    Controller controller = new Controller();

    public void consoleInterface(){
        System.out.println("Enter task name (a / b / c / d)");
        String fromUser= scan.next();

        while (!fromUser.equals("1")) {
            if (fromUser.equalsIgnoreCase("a")) {
                controller.taskASolution();
            } else if (fromUser.equalsIgnoreCase("b")) {
                controller.taskBSolution();
            } else if(fromUser.equalsIgnoreCase("c")){
                controller.taskCSolution();
            } else if(fromUser.equalsIgnoreCase("d")){
                controller.taskDSolution();
            }
            System.out.println("Enter task name (a / b / c / d)");
            System.out.println("Enter 1 to exit");
            fromUser = scan.next();
        }
    }
}
