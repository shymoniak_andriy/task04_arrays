package com.shymoniak.controller;

import com.shymoniak.model.MainModel;

public class Controller {
    MainModel mainModel = new MainModel();

    public void taskASolution() {
        mainModel.resTaskA();
    }

    public void taskBSolution() {
        mainModel.resTaskB();
    }

    public void taskCSolution(){
        mainModel.resTaskC();
    }
     public void taskDSolution(){
        mainModel.resTaskD();
     }
}
